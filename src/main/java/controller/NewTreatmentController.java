package controller;

import datastorage.DAOFactory;
import datastorage.NurseDAO;
import datastorage.TreatmentDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Patient;
import model.Nurse;
import model.Treatment;
import utils.DateConverter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * The <code>NewTreatmentController</code> contains the entire logic of the NewTreatmentView. The user determines inside this which data will be written to the database.
 */
public class NewTreatmentController {
    @FXML
    private Label lblSurname;
    @FXML
    private Label lblFirstname;
    @FXML
    private TextField txtBegin;
    @FXML
    private TextField txtEnd;
    @FXML
    private TextField txtDescription;
    @FXML
    private TextArea taRemarks;
    @FXML
    private DatePicker datepicker;
    @FXML
    private ComboBox<String> comboBox;

    private AllTreatmentController controller;
    private Patient patient;
    private Nurse nurse;
    private Stage stage;
    private NurseDAO dao;
    private ArrayList<Nurse> nurseList;
    private ObservableList<String> myComboBoxData =
            FXCollections.observableArrayList();
    private ObservableList<Nurse> tableviewContent =
            FXCollections.observableArrayList();

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding FXML file is to be displayed.
     */
    public void initialize(AllTreatmentController controller, Stage stage, Patient patient) {
        this.controller= controller;
        this.patient = patient;
        this.stage = stage;
        createComboBoxData();
        comboBox.setItems(myComboBoxData);
        showPatientData();
    }

    /**
     * puts the patients name in labels
     */
    private void showPatientData(){
        this.lblFirstname.setText(patient.getFirstName());
        this.lblSurname.setText(patient.getSurname());
    }

    /*
     *creates a new treatment
     */
    @FXML
    public void handleAdd(){
        /* the parameters of a treatment are set here */
        LocalDate date = this.datepicker.getValue();
        String s_begin = txtBegin.getText();
        LocalTime begin = DateConverter.convertStringToLocalTime(txtBegin.getText());
        LocalTime end = DateConverter.convertStringToLocalTime(txtEnd.getText());
        String description = txtDescription.getText();
        String remarks = taRemarks.getText();
        String n = this.comboBox.getSelectionModel().getSelectedItem();
        nurse = searchInList(n);    //the nurse in care is selected
        Treatment treatment = new Treatment(patient.getPid(), nurse.getNid(), date,
                begin, end, description, remarks, false);
        createTreatment(treatment);     // treatment is created
        controller.readAllAndShowInTableView();     //reload the table
        stage.close();
    }

    /*
     * the create function in DAOimp is called to create a new treatment
     */
    private void createTreatment(Treatment treatment) {
        TreatmentDAO dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        try {
            dao.create(treatment);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void handleCancel(){
        stage.close();
    }

    /**
     *the data for the comboBox filled with nurses is created
     */
    private void createComboBoxData(){
        NurseDAO dao = DAOFactory.getDAOFactory().createNurseDAO();
        try {
            nurseList = (ArrayList<Nurse>) dao.readAll();   // reads all nurses from the NurseDAO and puts them into an ArrayLists
            this.myComboBoxData.add("alle");
            for (Nurse nurse: nurseList) {
                this.myComboBoxData.add(nurse.getSurname());    // add the surname to the comboBox
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @FXML
    public void handleComboBox(){
        String n = this.comboBox.getSelectionModel().getSelectedItem();
        this.tableviewContent.clear();
        this.dao = DAOFactory.getDAOFactory().createNurseDAO();
        List<Nurse> allNurses;
        if(n.equals("alle")) {
            try {
                allNurses = this.dao.readAll();
                for (Nurse nurse : allNurses) {
                    this.tableviewContent.add(nurse);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * looks for the nurse with the matching surname in the ArrayList
     * @param surname
     * @return
     */
    private Nurse searchInList(String surname){
        for (int i =0; i<this.nurseList.size();i++){
            if(this.nurseList.get(i).getSurname().equals(surname)){
                return this.nurseList.get(i);
            }
        }
        return null;
    }
}