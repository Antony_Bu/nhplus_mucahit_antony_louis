package controller;

import datastorage.DAOFactory;
import datastorage.UsersDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.io.IOException;



import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import model.User;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class LoginController {

    @FXML
    private BorderPane mainBorderPane;

    @FXML
    TextField userNameInput;
    @FXML
    TextField passwordInput;
    @FXML
    Button btnSignin;

    private UsersDAO dao;
    Boolean allowAccess;

    @FXML
    private void handleCheckUser() {
        this.dao = DAOFactory.getDAOFactory().createUsersDAO();     // starting connection with DB
        boolean userNameFound = false;

        List<User> allUser;
        int keyIndex=0;

        try {
            allUser = dao.readAll();
            for (int i = 1; i <= allUser.size(); i++) {
                if (dao.read(i).getUsername().equals(this.userNameInput.getText())) {    // Checks if username exists
                    keyIndex+=i;
                    userNameFound = true;
                    break;
                }
                if (!dao.read(i).getUsername().equals(this.userNameInput.getText()) && i== allUser.size()){  // If username was not found clears textfields
                    userNameFound = false;
                    allowAccess=false;
                    userNameInput.clear();
                    passwordInput.clear();
                    JOptionPane.showMessageDialog(null, "User does not exist!");
                }

            }

            if (userNameFound) {
                if (dao.read(keyIndex).getPassword().equals(this.passwordInput.getText())) { // Checks if password is right
                    allowAccess = true;
                } else {
                    allowAccess = false;                                                       //  If Passowrd is worng clears password field
                    passwordInput.clear();
                    JOptionPane.showMessageDialog(null, "Wrong password!");
                }

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        if (allowAccess) {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/MainWindowView.fxml"));  // Grants access if username and password are right
            try {
                mainBorderPane.setCenter(loader.load());
                MainWindowController controller = loader.getController();

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }
}